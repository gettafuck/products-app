"use strict";

var productsApp = angular.module("productsApp", ["ngRoute"]);

// Globals ($rootScope)
productsApp.run(function($rootScope, $http, $location) {

	$rootScope.collections = {
		users: [],
		products: [],
		orders: []
	}

	$rootScope.previousValues = {};

	var logedUser = Cookies.get("logedUser");
	if(logedUser !== undefined)
		$rootScope.logedUser = JSON.parse( logedUser );
	else
		$rootScope.logedUser = {};
	console.log("\n - data of Loged User is:");
	console.log($rootScope.logedUser);


	var orderData = Cookies.get("orderData");
	if(orderData !== undefined) {
		$rootScope.orderData = JSON.parse( orderData );
	} else {
		$rootScope.orderData = {
			user: {},
			products: [],
			totalAmount: 0,
			totalCost: 0
		};
	}
	console.log("\n - data of Order is:");
	console.log($rootScope.orderData);



//--- CRUD Operations ---//
	$rootScope.get = function(collectionName) {
		$http({
			method: "GET",
			url: "/" + collectionName
		}).success(function(response) {
			console.log(" - Success for Get '" + collectionName + "' data.");
			console.log(response);
			$rootScope.collections[collectionName] = response;
		}).error(function(){
			console.log(" - Error for Get '" + collectionName + "' data.");
		});
	}

	$rootScope.save = function(obj, collectionName) {
		console.log("\ntrying to Save object ");
		console.log(obj);
		console.log(" to collection '" + collectionName + "'");

		$http({
			method:"POST",
			url: "/" + collectionName,
			data: obj 
		}).success(function(response) {
			console.log("server ansver is: " + response);
			var newObj = {};
			for( var item in obj ) {
				var propertyName = item;
				newObj[propertyName] = obj[propertyName];
			}
			console.log("newObj is:");
			console.log(newObj);
			newObj._id = response._id;
			$rootScope.collections[collectionName].push(newObj);
		}).error(function() {
			console.log("\n - Error for Save data. \n");
		});
	}

	$rootScope.update = function(obj, collectionName) {
		console.log("\ntrying to Update object with _id " + obj._id + " from collection '" + collectionName + "'");
		console.log("I will send new version of object to server: ");
		console.log(obj);

		$http({
			method: "PUT",
			url: "/" + collectionName + "/" + obj._id,
			data: obj
		}).success(function(response) {
			console.log( " - Success for Update data of object with id " + response._id);
			$("#edit-" + obj._id).find(".cancel-btn").fadeOut(200);
			$("#edit-" + obj._id).find(".close-btn").fadeIn(200);
		}).error(function(){
			console.log("\n - Error for Update data. \n");
		});
	}
	$rootScope.edit = function(obj) {
		console.log("\n - trying to Edit object with _id " + obj._id );
		$("#edit-" + obj._id).fadeIn(200);
		$("#info-" + obj._id).fadeOut(200);

		$("#edit-" + obj._id).find(".cancel-btn").fadeIn(200);
		$("#edit-" + obj._id).find(".close-btn").fadeOut(200);

		// save previous value of object
		$rootScope.previousValues[obj._id] = {};

		for( var item in obj) {
			var propertyName = item;
			$rootScope.previousValues[obj._id][propertyName] = obj[propertyName];
			console.log($rootScope.previousValues[obj._id][propertyName]);
		}
		console.log("Previous value of object with id '" + obj._id + "'' is:");
		console.log($rootScope.previousValues[obj._id]);
	}

	$rootScope.cancel = function(objId, collectionName, indexOfObj) {
		console.log("\n - Cancel for editing object with _id " + objId );
		$("#edit-" + objId).fadeOut(200);
		$("#info-" + objId).fadeIn(200);

		// override object with index 'indexOfObj', set previous value to this object
		$rootScope.collections[collectionName][indexOfObj] = $rootScope.previousValues[objId];
	}

	$rootScope.close = function(objId) {
		console.log("\n - Close edit-board for object with _id " + objId );
		$("#edit-" + objId).fadeOut(200);
		$("#info-" + objId).fadeIn(200);
	}

	$rootScope.delete = function(objId, collectionName) {
		console.log("\ntrying to Delete object with _id " + objId + " from collection '" + collectionName + "'");

		$http({
			method: "DELETE",
			url: "/" + collectionName + "/" + objId
		}).success(function(response) {
			console.log("\nSuccessfully Deleted object with _id '" + response._id + "' to collection '" + response.nameOfCollection + "'\n") ;
			$("#" + objId).fadeOut(200);
		}).error(function() {
			console.log("\n - Error for Delete data. \n");
		});
	}
//--- end CRUD Operations ---//



//--- USER Methods ---//
	$rootScope.userDataForLogin = {};
	$rootScope.userDataForRegister = {};
	$rootScope.resultOfUserChecking = {};
	$rootScope.User = {
		login: function(userData) {
			$http({
				method: "POST",
				url: "/login",
				data: userData
			}).success(function(response){
				if(response.allowToLogin === true) {
					Cookies.set("logedUser", response.userData);
					$rootScope.logedUser = response.userData;
					alert("Success for Login");
					location.replace("/#/home");
				} else
					alert("User is not existed or You input wrong Password.");
			}).error(function(){
				console.log("\n - Error for Login.");
			});
		},

		register: function(userData) {
			$http({
				method: "POST",
				url: "/register",
				data: userData
			}).success(function(response){
				if(response.allowToRegister === true) {
					alert("Success for Register");
					location.replace("/#/home");
				} else
					alert("User is already existed.");
			}).error(function(){
				console.log("\n - Error for Register.");
			});
		},

		exit: function() {
			$rootScope.logedUser = {};
			Cookies.remove("logedUser");
			$rootScope.orderData = {};
			Cookies.remove("orderData");
			location.reload();
		}
	};
//--- end USER Methods ---//



//--- PRODUCT Methods ---//
	$rootScope.addToCart = function(obj) {
		console.log("\n\n Added to Cart product-object with id " + obj._id);

		$rootScope.orderData.user = $rootScope.logedUser;

		obj.amount = 1;

		var length = $rootScope.orderData.products.length;

		if(length < 1) {
			$rootScope.orderData.products[length] = {};
			for(var propertyName in obj) {
				$rootScope.orderData.products[length][propertyName] = obj[propertyName];
			}
			console.log("\n Added product: ");
			console.log($rootScope.orderData.products[length]);
		}
		else {
			for(var i = 0; i < length; i++) {
				if(obj._id !== $rootScope.orderData.products[i]._id) {
					$rootScope.orderData.products[length] = {};
					for(var propertyName in obj) {
						$rootScope.orderData.products[length][propertyName] = obj[propertyName];
					}
					console.log("\n Added product: ");
					console.log($rootScope.orderData.products[length]);
				}
				else {
					$rootScope.orderData.products[i].amount++;
					console.log("\n Added product: " + $rootScope.orderData.products[i].name);
				}
			}
		}

		$rootScope.orderData.totalCost += (obj.cost*1);
		$rootScope.orderData.totalAmount++;

		console.log("\n - Order data is:");
		console.log($rootScope.orderData);

		Cookies.set("orderData", $rootScope.orderData);
	}
//--- end PRODUCT Methods ---//



//--- ORDER Methods ---//
	$rootScope.saveOrder = function(obj) {
		$http({
			method: "POST",
			url: "/orders",
			data: obj
		}).success(function() {
			alert("Your Order was Successfully Saved. We will contact with You.");
		}).error(function() {
			alert("Cannot Save Your Order. Cannot connect to server.");
		})
	}
//--- end ORDER Methods ---//



//--- Interface-Only Methods ---//
	$rootScope.closeLayer = function() {
	    $(".close-layer").removeClass("active-close-layer");
		$(".side-menu").removeClass("active-side-menu");

	    setTimeout(function() {
	        $(".pop-up-wrapper").css("display", "none");
	        $(".side-menu-wrapper").css("display", "none");
	    }, 500);  
	}

	$rootScope.showPopUp = function() {
		$(".pop-up-wrapper").css("display", "block");
		setTimeout(function() {
			$(".close-layer").addClass("active-close-layer");
		}, 1);
	}

	$rootScope.showMenu = function() {
	    $(".side-menu-wrapper").css("display", "block");
	    setTimeout(function() {
	        $(".close-layer").addClass("active-close-layer");
	        $(".side-menu").addClass("active-side-menu");
	    }, 1);
	}
	
	// target - html-attr 'id' of element
	// time - time in miliseconds
	$rootScope.scrollTo = function(target, time) {
		if(!time) time = 300;
		target = "#" + target;
		$('html, body').animate({scrollTop: $(target).offset().top}, time);
		return false;
	};
//--- end Interface-Only Methods ---//



//--- OTHE Methods ---//
	// Sorting / Order by
	$rootScope.propertyName = undefined;
	$rootScope.byReducing = undefined;
	$rootScope.clickedFrom = undefined;

	$rootScope.sortSwitcher = function(propertyName) {
		$rootScope.propertyName = propertyName;
		$rootScope.byReducing = !$rootScope.byReducing;
		$rootScope.clickedFrom = propertyName;
	}
//--- end OTHE Methods ---//

}); // end of $rootScope



// Routes
	productsApp.config(["$routeProvider", function($routeProvide) {
		$routeProvide.
		when("/home", {
			templateUrl: "templates/home.html",
			controller: "homeController"
		}).
		when("/users", {
			templateUrl: "templates/users.html",
			controller: "usersController"
		}).
		when("/products", {
			templateUrl: "templates/products.html",
			controller: "productsController"
		}).
		when("/orders", {
			templateUrl: "templates/orders.html",
			controller: "ordersController"
		}).
		when("/order", {
			templateUrl: "templates/order.html",
			controller: "orderController"
		}).
		when("/login", {
			templateUrl: "templates/login.html",
			controller: "loginController"
		}).
		when("/register", {
			templateUrl: "templates/register.html",
			controller: "registerController"
		}).
		otherwise({
			redirectTo: "/home"
		});
	}]);
// end Routes



//--- CONTROLLERS ---//
	// HOME Controller
	productsApp.controller("homeController", ["$scope", "$http", "$location", function($scope, $http, $location) {
	} ] );


	// USERS Controller
	productsApp.controller("usersController", ["$scope", "$http", "$location", function($scope, $http, $location) {
		console.log("User-role is " + $scope.logedUser.role);

		// name of collection with data in DB
		$scope.nameOfCollection = "users";

		if($scope.logedUser.role === "admin")
			$scope.get( $scope.nameOfCollection );
		else {
			alert("Sorry, this page is available only for admin.");
			setTimeout(function(){
				$location.path("/login");
			}, 1);
		}

	} ] );


	// PRODUCTS Controller
	productsApp.controller("productsController", ["$scope", "$http", "$location", function($scope, $http, $location) {
		console.log("User-role is " + $scope.logedUser.role);
		// name of collection with data in DB
		$scope.nameOfCollection = "products";
		$scope.get( $scope.nameOfCollection );
	} ] );


	// ORDERS Controller
	productsApp.controller("ordersController", ["$scope", "$http", "$location", function($scope, $http, $location) {
		console.log("User-role is " + $scope.logedUser.role);

		// name of collection with data in DB
		$scope.nameOfCollection = "orders";

		if($scope.logedUser.role === "admin")
			$scope.get( $scope.nameOfCollection );
		else {
			alert("Sorry, this page is available only for admin.");
			setTimeout(function(){
				$location.path("/login");
			}, 1);
		}

	} ] );


	// ORDER Controller
	productsApp.controller("orderController", ["$scope", "$http", "$location", function($scope, $http, $location) {
	} ] );


	// LOGIN Controller
	productsApp.controller("loginController", ["$scope", "$http", "$location", function($scope, $http, $location) {
	} ] );


	// REGISTER Controller
	productsApp.controller("registerController", ["$scope", "$http", "$location", function($scope, $http, $location) {
	} ] );

