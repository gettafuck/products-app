'use strict';

var Config = {
	dev: {
		server: {
			protocol: 'http://',
			adress: 'localhost',
			port: 3312
		},
		db: {
			adress: 'mongodb://localhost',
			port: 27017,
			name: 'test'
		}
	},
	production: {
		server: {
			protocol: 'http://',
			adress: 'productsapp-94079.onmodulus.net/',
			port: 3312
		},
		db: {
			adress: 'mongodb://modulusmongo.net',
			port: 27017,
			name: 'vU9dapem'
		}
	}
};

module.exports = Config.dev;


/*

MONGO URI
mongodb://<user>:<pass>@jello.modulusmongo.net:27017/vU9dapem
MONGO CONSOLE
mongo jello.modulusmongo.net:27017/vU9dapem -u <user> -p <pass>

*/