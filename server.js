"use strict";

// required-modules.js
	var mongoose = require("mongoose");
	var express = require("express");
	var bodyParser = require("body-parser");

	var config = require('./config');

// db.js
	mongoose.connect(config.db.adress + ':' + config.db.port + '/' + config.db.name);

	var db = mongoose.connection;
	db.on('error', console.error.bind(console, 'connection error:'));
	db.once('open', function (callback) {
		console.log("\n");
		console.log("Successfully connected to _DB_ \n" + config.db.adress + ':' + config.db.port + '/' + config.db.name);
	});

// Schemes
	var productSchema = mongoose.Schema({
	    imgUrl: String,
	    name: String,
	    cost: Number,
	    description: String,
	    label: String,
	    isInStore: Boolean
	});
	var userSchema = mongoose.Schema({
	    imgUrl: String,
	    name: String,
	    role: String,
	    login: String,
	    password: String,
	    mail: String,
	    phone: String
	});

	var orderSchema = mongoose.Schema({
		user_id: String,
		products: [{
			product_id: String,
			amount: Number,
			totalProductCost: Number
		}],
		totalOrderCost: Number
	});

// Models (Entities in Database)
	var Product = mongoose.model('Product', productSchema);
	var User = mongoose.model('User', userSchema);
	var Order = mongoose.model('Order', orderSchema);




// routes.js
const app = express();
const server = app.listen(config.server.port, () => {
	console.log('Server listening at ' +
		config.server.protocol +
		config.server.adress +
		':' + config.server.port);
});

app.use( express.static(__dirname+"/public") );
app.use( bodyParser.json() );



// Routes (processing of requests from client)

// Users
	app.get("/users", function (request, response) {
		console.log("\n\n [•] recieved a GET-reqest from client.");
		console.log("\n Client wants to get users-data.");

		User.find( function (error, users) {
			console.log("\n - Successfully Found users. \n\n");
			response.send(users);
		} );

		// response.send(testUsers);
	});
	app.post("/users", function (request, response) {
		console.log("\n\n [•] recieved a POST-request from client. \n ");
		console.log("I will Save the next user-object: \n");
		console.log(request.body);

		var user = new User(request.body);
		user.save( function (error, user) {
			console.log("\n - Successfully Saved object with id: " + request.body._id);
			response.send( {_id: user._id} );
		} );

	});
	app.put("/users/:objId", function (request, response) {
		console.log("\n\n [•] recieved a PUT-request from client. \n ");
		console.log("I will Update user-object with id " + request.params.objId);
		console.log(request.body);

		var id = request.params.objId;
		User.update({_id: id}, request.body, function(error) {
			response.send( {_id: request.params.objId} );
		});
	});
	app.delete("/users/:objId", function (request, response) {
		console.log("\n\n [•] recieved a DELETE-request from client. \n ");
		console.log("I will Remove user-object with id " + request.params.objId);
		var objId = request.params.objId;

		User.remove({_id: request.params.objId}, function(error, post) {
			console.log("\n - Successfully Deleted object with id: " + request.params.objId);
			response.send({_id: objId, nameOfCollection: "users"});
		});

	});



// Products
	app.get("/products", function (request, response) {
		console.log("\n\n [•] recieved a GET-reqest from client.");
		console.log("\n Client wants to get products-data.");

		Product.find( function (error, products) {
			console.log("\n - Successfully Found products. \n\n");
			response.send(products);
		} );

		// response.send(testProducts);
	});
	app.post("/products", function (request, response) {
		console.log("\n\n [•] recieved a POST-request from client. \n ");
		console.log("I will Save the next product-object: \n");
		console.log(request.body);

		var product = new Product(request.body);
		product.save(function (error, product) {
			console.log("\n - Successfully Saved object");
			console.log(request.body);
		});

		// response.send( {_id: "NewObjId_65sdf565"} );
	});
	app.put("/products/:objId", function (request, response) {
		var objId = request.params.objId;

		console.log("\n\n [•] recieved a PUT-request from client. \n ");
		console.log("I will Update product-object with id " + objId);
		console.log(request.body);

		Product.update({_id: objId}, request.body, function(error) {
			response.send( {_id: objId} );
		});
	});
	app.delete("/products/:objId", function (request, response) {
		var objId = request.params.objId;

		console.log("\n\n [•] recieved a DELETE-request from client. \n ");
		console.log("I will Remove product-object with id " + objId);

		Product.remove({_id: objId}, function(error, post) {
			console.log("\n - Successfully Deleted object with id: " + objId);
			response.send({_id: objId, nameOfCollection: "users"});
		});
	});



// Login / Register
	app.post("/login", function(request, response) {
		var users;
		User.find( function (error, docs) {
			console.log("\n - Successfully Found users. \n\n");
			users = docs;

			var checkingUser = request.body;
			var serverAnswer = {};

			for(var i = 0; i < users.length; i++) {
				if( checkingUser.login === users[i].login && checkingUser.password === users[i].password ) {
					serverAnswer.allowToLogin = true;
					serverAnswer.userData = users[i];
					break;
				} else
					serverAnswer.allowToLogin = false;
			}

			if(serverAnswer.allowToLogin === true) {
				console.log("\n - Success for Login.");
			} else
				console.log("\n - Login Denied. User is not existed or Password is wrong.");

			response.send(serverAnswer);
		} );
	});

	app.post("/register", function(request, response) {
		var users;
		User.find( function (error, docs) {
			console.log("\n - Successfully Found users. \n\n");
			users = docs;

			var checkingUser = request.body;
			console.log("\n - Recieved a POST-request with user data for register:");
			console.log(checkingUser);
			var serverAnswer = {};

			for(var i = 0; i < users.length; i++) {
				if( checkingUser.login === users[i].login ) {
					serverAnswer.allowToRegister = false;
					break;
				} else
					serverAnswer.allowToRegister = true;
			}
			if(serverAnswer.allowToRegister === true) {
				console.log("\n - Success for Register.");
				var user = new User(request.body);
				user.save( function (error, doc) {
					console.log("\n - Successfully Saved new user:");
					console.log(doc);
				} );
			} else
				console.log("\n - Register Denied. User is already existed.");

			response.send(serverAnswer);
		} );
	});



// Orders
	app.get("/orders", function (request, response) {
		console.log("\n\n [•] recieved a GET-reqest from client.");
		console.log("\n Client wants to get orders-data.");

		Order.find(function (error, orders) {
			console.log("\n - Successfully Found orders. \n\n");
			response.send(orders);
		});

	});

	app.post("/orders", function (request, response) {
		console.log("\n\n [•] recieved a POST-request from client. \n ");
		console.log("I will Save the next order-object: \n");
		console.log(request.body);

		var order = new Order(request.body);
		order.save(function(error, doc) {
			console.log("\n - Successfully Saved object.");
			console.log(doc);
			response.send("Success for Save new Order.");
		});

	});

	app.put("/orders/:objId", function (request, response) {
		var objId = request.params.objId;

		console.log("\n\n [•] recieved a PUT-request from client. \n ");
		console.log("I will Update order-object with id " + objId);
		console.log(request.body);

		Order.update({_id: objId}, request.body, function(error) {
			response.send( {_id: objId} );
		});
	});

	app.delete("/orders/:objId", function (request, response) {
		var objId = request.params.objId;

		console.log("\n\n [•] recieved a DELETE-request from client. \n ");
		console.log("I will Remove order-object with id " + objId);

		Order.remove({_id: objId}, function(error, order) {
			console.log("\n - Successfully Deleted object with id: " + objId);
			response.send({_id: objId, nameOfCollection: "orders"});
		});
	});